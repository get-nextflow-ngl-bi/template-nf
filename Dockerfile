FROM nfcore/base:1.7
LABEL authors="Céline Noirot" \
      description="Docker image containing all requirements for get/template pipeline"

COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a
ENV PATH /opt/conda/envs/GeT-template-1.0dev/bin:$PATH
