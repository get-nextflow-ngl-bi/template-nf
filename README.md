
[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A50.32.0-brightgreen.svg)](https://www.nextflow.io/)

[![pipeline status](https://forgemia.inra.fr/get-nextflow-ngl-bi/template-nf/badges/master/pipeline.svg)](https://forgemia.inra.fr/get-nextflow-ngl-bi/template-nf//-/commits/master)


# Ce repository est un template pour les workflows Get

Ce workflow et ses différentes configurations permettent :
- d'executer un pipeline a partir d'un fichier samples.csv
- d'utiliser une image singularity ou conda ou path (cf profils)
- d'executer un multiqc 
- de tracer les versions des logiciels
- d'envoyer un email à la fin du pipeline --email toto@fai.fr
- de générer automatiquement une image singularity et de la mettre a disposition dans le registry de la forge.

## Comment utiliser ce répository ?

Cloner le repo
```
git clone git@forgemia.inra.fr:get-nextflow-ngl-bi/template-nf.git
```

Voici la liste des fichiers a récupérer avec leur utilité :
- `asset` code pour email et config de multiQC
- `conf` configurations utilisées dans `nextflow.config`    
    - base : conf générale
    - path : si profile utilisé est --multipath ajouter un block par process ayant des dépendances
    - test : chaque pipeline devra avoir un profil de test pour tester les pipelines
    - genomes : devra peut-etre etre centralisé ailleurs pour avoir un seul fichier contenant les genomes utilisés par la pf.

- `doc/output.md` : ce fichier devra etre copié et modifié avec la description des outputs du pipeline. Ce fichier est ensuite converti en html dans le repertoires de resultats du pipelines.

- `.gitlab-ci.yml` si vous souhaitez avoir la génération automatique de l'image singularity à partir des fichiers `Singularityfile` et `environment.yml` mettez ce fichier à la racine de votre projet. L'image sera ensuite recupérable avec la commande suivante :
```
singularity pull template-nf.sif oras://registry.forgemia.inra.fr/get-nextflow-ngl-bi/template-nf/template-nf:latest
```

- les fichiers `CHANGELOG.md`, `LICENCE`, `README.md` a utiliser et modifier

- `main.nf` : le pipeline
- `nextflow.config` : la conf générale du pipeline
- pour le reproductibilité : `Singularityfile` et `environment.yml` (si besoin en plus: `Dockerfile`)

## Et apres ?
- nomenclature: les channels doivent etre nommée comme suis: ch_FILE1_for_PROCESS_DESTINATION
- mettre en place des données de tests
- lorsque l'on code un process : 
    - utiliser les labels (pour la memoire, cpu, temps) définis dans base.config
    - ajouter les logiciels utilisés dans get_software_versions
- documenter le quick start ci-dessous et supprimer le paragraphe 'Ce repository est un template pour les workflows Get'
- completer le `doc/output.md` et le `doc/usage.md`
- tagger un pipeline dès que les fonctionnalités attendues sont codées



> La documentation suivante est a modifier et a garder. La precedente est a supprimer. 

## Introduction

The pipeline is built using [Nextflow](https://www.nextflow.io), a workflow tool to run tasks across multiple compute infrastructures in a very portable manner. It comes with docker and singularity containers making installation trivial and results highly reproducible.

## Quick Start

i. Install [`nextflow`](https://nf-co.re/usage/installation)

ii. Install one of [`singularity`](https://www.sylabs.io/guides/3.0/user-guide/) or [`conda`](https://conda.io/miniconda.html)

iii. Clone the pipeline and download the singularity pipeline 

```bash
git clone git@forgemia.inra.fr:get-nextflow-ngl-bi/template-nf.git
cd template-nf
singularity pull template-nf.sif oras://registry.forgemia.inra.fr/get-nextflow-ngl-bi/template-nf/template-nf:latest
```
iv. Run the pipeline

```bash
nextflow run pathto/template-nf/main.nf -profile test,singularity
```

