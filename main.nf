#!/usr/bin/env nextflow

nextflow.enable.dsl = 2

/*
Copyright INRAE 2021

This software is a computer program whose purpose is to
analyze high-throughput sequencing data.
You can use, modify and/ or redistribute the software under the terms
of license (see the LICENSE file for more details).
The software is distributed in the hope that it will be useful,
but "AS IS" WITHOUT ANY WARRANTY OF ANY KIND.
Users are therefore encouraged to test the software's suitability as regards
their requirements in conditions enabling the security of their systems and/or data.
The fact that you are presently reading this means that you have had knowledge
of the license and that you accept its terms.
This script is based on : 
 - the nf-core guidelines . See https://nf-co.re/ for more information
 - the Curie institute template https://github.com/bioinfo-pf-curie/geniac-template/

*/

/*
========================================================================================
    NAMED WORKFLOW FOR PIPELINE
========================================================================================
*/

include { techno_qc } from './workflow/techno_qc.nf'

workflow QC_ANALYSIS {
    techno_qc()
}

/*
========================================================================================
    RUN ALL WORKFLOWS
========================================================================================
*/

workflow {
    QC_ANALYSIS()
}
