#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process workflow_summary {
  
  output:
  path('workflow_summary_mqc.yaml')

  exec:
  def yaml_file = task.workDir.resolve('workflow_summary_mqc.yaml')
  yaml_file.text  = """
  id: 'summary'
  description: " - this information is collected when the pipeline is started."
  section_name: 'Workflow Summary'
  section_href: "${workflow.manifest.homePage}"
  plot_type: 'html'
  data: |
        <dl class=\"dl-horizontal\">
  ${params.summary.collect { k,v -> "            <dt>$k</dt><dd><samp>${v ?: '<span style=\"color:#999999;\">N/A</a>'}</samp></dd>" }.join("\n")}
        </dl>
  """.stripIndent()
}

/*
 * STEP - MultiQC
 */
process multiqc {
  
  publishDir "${params.outdir}/MultiQC", mode: 'copy'

  module 'bioinfo/MultiQC-v1.7'

  when:
  !params.skip_multiQC

  input:
  path(multiqc_config)
  path('fastqc/*') //.collect().ifEmpty([])
  // TODO get-nf: Add in log files from your new processes for MultiQC to find!
  //path('software_versions/*')	//.collect()
  path('workflowSummary/*')	//.collect()

  output: 
  path("*report.html")
  path("*_data")
  path("multiqc_plots")

  script:
  rtitle = params.custom_runName ? "--title \"$params.custom_runName\"" : ''
  rfilename = params.custom_runName ? "--filename " + params.custom_runName.replaceAll('\\W','_').replaceAll('_+','_') + "_multiqc_report" : ''
  """
  multiqc -f $rtitle $rfilename  --config $multiqc_config .
  """
}

/*
 * STEP - Output Description HTML
 */
process output_documentation {
    publishDir "${params.outdir}/pipeline_info", mode: 'copy'
    
    module 'system/pandoc-2.1.3'

    input:
    path(output_docs)

    output:
    path("results_description.html")

    script:
    """
    pandoc $output_docs -t html -o results_description.html
    """
}