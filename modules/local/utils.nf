/*
 * Parse software version numbers
 */
process get_software_versions {
    publishDir "${params.outdir}/pipeline_info", mode: 'copy',
        saveAs: { filename ->
            if (filename.indexOf(".csv") > 0) filename
            else null
        }

    output:
    path('software_versions_mqc.yaml')
    path("software_versions.csv")

    script:
    // TODO nf-core: Get all tools to print their version number here
    """
    echo $workflow.manifest.version > v_pipeline.txt
    echo $workflow.nextflow.version > v_nextflow.txt
    fastqc --version > v_fastqc.txt
    multiqc --version > v_multiqc.txt
    scrape_software_versions.py &> software_versions_mqc.yaml
    """
}

/*
 * STEP 1 - FastQC
 */
process fastqc {
    tag "$name"
    label 'process_medium'
    publishDir "${params.outdir}/fastqc", mode: 'copy',
        saveAs: { filename -> filename.indexOf(".zip") > 0 ? "zips/$filename" : "$filename" }

	module 'bioinfo/FastQC_v0.11.7'
	
    input:
    tuple val(name), path(reads)

    output:
    path "*_fastqc.{zip,html}"

    script:
    """
    fastqc --quiet --threads $task.cpus $reads
    """
}

/*
 * STEP 2 - Fake QC
 */
process qc1 {
	module 'bioinfo/FastQC_v0.11.7'

    input:
    tuple val(replicate_id), path(reads)
	
    output:
    path("${replicate_id}.qc1")

    script:
    """
        echo "mkdir ${replicate_id} ; fastqc --nogroup --quiet -o ${replicate_id} --threads ${task.cpus} ${reads[0]} ${reads[1]}" > ${replicate_id}.qc1
    """
}

/*
 * STEP 3 - Fake assembly
 */
process assembly {
    input:
   	path(qc)
    tuple val(replicate_id), path(reads)
    
    output:
    path("${replicate_id}.assembly")

    script:
    """
        echo "ASSEMBLY ${replicate_id} ; " > ${replicate_id}.assembly
    """
}
