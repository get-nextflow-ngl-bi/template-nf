#!/usr/bin/env nextflow

nextflow.enable.dsl=2

include {	workflow_summary; 
			multiqc; 
			output_documentation; } from '../../modules/local/modules_report.nf'

workflow report {
	take:
		ch_output_docs
		
	main:
	
		workflow_summary()
		//multiqc(ch_multiqc_config, fastqc.out.collect().ifEmpty([]), workflow_summary.out.collect())
		output_documentation(ch_output_docs)
	
}